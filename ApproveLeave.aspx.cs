﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data;
using System.Text;
using System.Configuration;

public class ListHolidayRequests
{
    public DateTime HolidayDate { get; set; }
    public char HolidayType { get; set; }       // H = Half Day; F = Full Day
    public int HolidayApproved { get; set; }    // 1 = Approved; 0 = Not Approved
}
public class ListMinions
{
    public int MinionID { get; set; }
    public string MinionFirstName { get; set; }
    public string MinionFullName { get; set; }
    public string MinionEmail { get; set; }
}

namespace MobacarLeave
{
    public partial class _Approve : System.Web.UI.Page
    {
        public static List<ListMinions> MinionList = new List<ListMinions>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Verify User; otherwise boot them to MemberLogin
            if (Session["LoggedIn"] != null)
            {
                if (!(bool)Session["LoggedIn"])
                { Response.Redirect("~/MemberLogin.aspx"); }
            }
            else { Response.Redirect("~/MemberLogin.aspx"); }

            // Update Master Header
            Master.FindControl("LogOut").Visible = true;
            Master.FindControl("btnApproveHolidays").Visible = false;
            Master.FindControl("btnReturn").Visible = true;
            Master.MasterReturn.Click += btnReturn_Click;
            Master.MasterPageLabel = "Mobacar Leave Management - Approve Leave";

            // Reset Calendar
            MobacarLeave.Controls.ApproveHolidays.UserID = Convert.ToInt32(cboEmployeeList.SelectedValue == "" ? "0" : cboEmployeeList.SelectedValue);
            
            if (!Page.IsPostBack)
            {
                // Do Stuff
                fillEmployeeList();
                fillLeaveTypeList();
            }
        }

        protected void fillEmployeeList()
        {
            int UserID = Convert.ToInt32(Session["UserID"]);

            // Get All Employees
            DataQueries dqGetEmployees = new DataQueries();
            DataTable dtEmployees = dqGetEmployees.GetStaff(UserID, "Employees");

            if (dtEmployees != null)
            {
                cboEmployeeList.Items.Clear();

                for (int i = 0; dtEmployees.Rows.Count > i; i++)
                {
                    MinionList.Add(new ListMinions
                    {
                        MinionID = Convert.ToInt32(dtEmployees.Rows[i]["ID"].ToString()),
                        MinionFirstName = dtEmployees.Rows[i]["FirstName"].ToString(),
                        MinionFullName = dtEmployees.Rows[i]["FullName"].ToString(),
                        MinionEmail = dtEmployees.Rows[i]["Email"].ToString()
                    });

                    ListItem cboItm = new ListItem(
                        dtEmployees.Rows[i]["FullName"].ToString(),
                        dtEmployees.Rows[i]["ID"].ToString()
                        );
                    cboEmployeeList.Items.Add(cboItm);
                }

                // Initial value
                cboEmployeeList.Items.Insert(0, new ListItem("Please select a Minion...", "0"));
            }
        }

        protected void cboEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboEmployeeList.SelectedValue != "0")
            {
                int EmployeeID = Convert.ToInt32(cboEmployeeList.SelectedValue);

                DataQueries dqEmployeeAllowance = new DataQueries();
                DataTable dtEmployeeAllowance = dqEmployeeAllowance.GetStaffRequests(EmployeeID, DateTime.Now.Year);

                // Set Calendar Variables
                MobacarLeave.Controls.ApproveHolidays.UserID = EmployeeID;
                MobacarLeave.Controls.ApproveHolidays.LeaveType = cboLeaveTypeList.SelectedValue.ToString();
                MobacarLeave.Controls.ApproveHolidays.AnnualAllowance = Convert.ToDouble(dtEmployeeAllowance.Rows[0]["AnnualAllowance"]);
                MobacarLeave.Controls.ApproveHolidays.LeaveRemaining = Convert.ToDouble(dtEmployeeAllowance.Rows[0]["AllowanceRemaining"]);
                MobacarLeave.Controls.ApproveHolidays.LeavePending = Convert.ToDouble(dtEmployeeAllowance.Rows[0]["LeaveOnRequest"]);

                // Refresh Calendar
                MobacarLeave.Controls.ApproveHolidays.listApprovedLeave.Clear();
                MobacarLeave.Controls.ApproveHolidays.GetRequestedHolidays();
            }
        }

        protected void fillLeaveTypeList()
        {
            // Get All Leave Types
            DataQueries dqLeaveTypes = new DataQueries();
            DataTable dtLeaveTypes = dqLeaveTypes.GetLeaveTypes();

            if (dtLeaveTypes != null)
            {
                cboLeaveTypeList.Items.Clear();
                for (int i = 0; dtLeaveTypes.Rows.Count > i; i++)
                {
                    ListItem cboItm = new ListItem(
                        dtLeaveTypes.Rows[i]["Description"].ToString(),
                        dtLeaveTypes.Rows[i]["Abbreviation"].ToString()
                        );
                    cboLeaveTypeList.Items.Add(cboItm);
                }

                // Initial value
                cboLeaveTypeList.Items.Insert(0, new ListItem("Holiday Requests", "0"));
            }
        }

        protected void cboLeaveTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string LeaveType = cboLeaveTypeList.SelectedValue.ToString();
            MobacarLeave.Controls.ApproveHolidays.LeaveType = LeaveType;
        }

        protected void btnApproveHolidayRequest_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListHolidayRequests> ListApprovedHolidays = MobacarLeave.Controls.ApproveHolidays.listApprovedLeave;

                // Save Approvals to DB
                SaveHolidays(ListApprovedHolidays);

                // Send Email to Employee if Holidays included in list; don't send for Sick LEave, etc.
                bool SendEmail = false;
                for (int i = 0; i < ListApprovedHolidays.Count; i++)
                {
                    string HolidayType = ListApprovedHolidays[i].HolidayType.ToString();
                    if (HolidayType == "D" || HolidayType == "H" || HolidayType == "F")
                    {
                        SendEmail = true;
                    }
                }
                if (SendEmail)
                {
                    SendEmails(ListApprovedHolidays);
                }

                // Update Existing Holidays with Selected Holidays, and then clear list
                MobacarLeave.Controls.ApproveHolidays.listRequestedHolidays.AddRange(MobacarLeave.Controls.ApproveHolidays.listApprovedLeave);
                MobacarLeave.Controls.ApproveHolidays.listApprovedLeave.Clear();
                MobacarLeave.Controls.ApproveHolidays.UserID = 0;
                MobacarLeave.Controls.ApproveHolidays.GetRequestedHolidays();   // Refresh

                // Reset Employee List
                cboEmployeeList.Items.Clear();
                fillEmployeeList();

                // Alert User
                if (SendEmail) {
                    PopUp("Leave Saved, and email sent to employee.");
                }
                else {
                    PopUp("Leave Saved");
                }
            }
            catch (Exception ex)
            {
                // Output Error Message
            }
        }
        
        protected void SaveHolidays(List<ListHolidayRequests> ApprovedHolidays)
        {
            int EmployeeID = Convert.ToInt32(cboEmployeeList.SelectedValue);
            int ManagerID = Convert.ToInt32(Session["UserID"]);

            for (int i = 0; i < ApprovedHolidays.Count; i++)
            {
                // Collect Details
                DateTime HolidayDate = ApprovedHolidays[i].HolidayDate;
                char HolidayType = ApprovedHolidays[i].HolidayType;
                int HolidayApproved = ApprovedHolidays[i].HolidayApproved;

                // Save Content
                DataQueries dqHoliday = new DataQueries();
                DataTable dtHoliday = dqHoliday.SaveHoliday(
                                          EmployeeID
                                        , HolidayDate
                                        , HolidayType
                                        , HolidayApproved
                                        , ManagerID);
            }
        }

        private int FindMinion(List<ListMinions> inList, int MinionID)
        {
            int retIndex = -1; // If not found return -1
            try
            {
                ListMinions Temp = inList.FirstOrDefault<ListMinions>(Minion => Minion.MinionID == MinionID);

                // If not found, Temp will be null, null has an index of -1
                retIndex = inList.IndexOf(Temp);

                return retIndex;
            }
            catch
            {
                return retIndex;
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/AddLeave.aspx");
        }

        protected void SendEmails(List<ListHolidayRequests> ApprovedLeave)
        {
            try
            {
                // Include HR Manager
                string HRManagerEmail = ConfigurationManager.AppSettings["HRManager"].ToString(); ;

                // Get Employee Detail
                int EmployeeID = Convert.ToInt32(cboEmployeeList.SelectedValue);
                var indexEmployee = FindMinion(MinionList, EmployeeID);
                string EmployeeEmail = MinionList[indexEmployee].MinionEmail;
                string EmployeeName = MinionList[indexEmployee].MinionFirstName;
                string EmployeeFullName = MinionList[indexEmployee].MinionFullName;

                // Get Manager Detail
                string ManagerEmail = Session["EmailAddress"].ToString();
                string ManagerName = Session["FirstName"].ToString() + " " + Session["LastName"].ToString();

                // Get Employee Leave Details
                double LeaveAllowance = MobacarLeave.Controls.ApproveHolidays.LeaveRemaining;
                double LeavePending = MobacarLeave.Controls.ApproveHolidays.LeavePending;

                // Set up Email
                string EmailFrom = "itadmin@mobacar.com";
                string EmailTo = EmployeeEmail + "," + ManagerEmail;
                if (ManagerEmail != HRManagerEmail) { EmailTo = EmailTo + "," + HRManagerEmail; } // So HR Manager doesn't get Duplicate Emails
                string EmailSubject = "Leave Request for " + EmployeeFullName;

                StringBuilder EmailBody = new StringBuilder();
                EmailBody.AppendLine("<p>Hi " + EmployeeName + ",</p>");
                EmailBody.AppendLine("<p>Regarding your requested Holidays:");

                // Sort list
                ApprovedLeave.Sort((Leave1, Leave2) => DateTime.Compare(Leave1.HolidayDate, Leave2.HolidayDate));
                for (int i = 0; i < ApprovedLeave.Count; i++)
                {
                    // Collect Details
                    string HolidayDate = ApprovedLeave[i].HolidayDate.ToString("dddd, dd/MMM/yyyy");
                    string HolidayType = ApprovedLeave[i].HolidayType.ToString();
                    int HolidayApproved = ApprovedLeave[i].HolidayApproved;

                    // Add Holidays
                    if ((HolidayApproved == 1) && (HolidayType == "H" || HolidayType == "F"))
                    {
                        EmailBody.AppendLine("<br/>Your Holiday for " + HolidayDate + " has been approved.");
                    }
                    else if (HolidayType == "D")
                    {
                        EmailBody.AppendLine("<br/>Your Holiday for " + HolidayDate + " has NOT been approved.");
                    }
                }

                EmailBody.AppendLine("</p><p>Number of Days Entitlement remaining: " + LeaveAllowance.ToString() + "</p>");
                EmailBody.AppendLine("<p>Thanks,<br/>" + ManagerName + "</p>");

                // Make and Send Email
                MailMessage mailObj = new MailMessage(EmailFrom, EmailTo, EmailSubject, EmailBody.ToString());
                mailObj.IsBodyHtml = true;

                SmtpClient SMTPServer = new SmtpClient("localhost");
                //SMTPServer.Credentials = new NetworkCredential("itadmin@mobacar.com", "1T_@dm!n20143");
                SMTPServer.UseDefaultCredentials = true;
                SMTPServer.Send(mailObj);
            }
            catch (Exception ex)
            {
                // Output Error Message
                string temp = ex.ToString();
                //PopUp(temp);
            }
        }

        protected void PopUp(string Message)
        {
            Response.Write("<script>alert('" + Message + "');</script>");
        }
    }
}