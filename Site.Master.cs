﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MobacarLeave
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public String MasterPageLabel
        {
            // MasterPageLabel is editable in the EditContent.aspx.cs page.
            get { return headerLabel.Text; }
            set { headerLabel.Text = value; }
        }

        public Button MasterReturn
        {
            get { return btnReturn; }
        }

        public Button MasterApproveHolidays
        {
            get { return btnApproveHolidays; }
        }

        public Button MasterEditEmployee
        {
            get { return btnEditEmployee; }
        }

        //public String MasterHomePageLink
        //{
        //    // MasterPageLink is editable in the EditContentFlatPage.aspx.cs page.
        //    get { return BackToHomePageFromHomePage.NavigateUrl; }
        //    set { BackToHomePageFromHomePage.NavigateUrl = value; }
        //}

        //public String MasterCountryPageLink
        //{
        //    // MasterPageLink is editable in the EditContentFlatPage.aspx.cs page.
        //    get { return LinkToPageFromCountry.NavigateUrl; }
        //    set { LinkToPageFromCountry.NavigateUrl = value; }
        //}

        //public String MasterLocationPageLink
        //{
        //    // MasterPageLink is editable in the EditContentFlatPage.aspx.cs page.
        //    get { return LinkToPageFromLocation.NavigateUrl; }
        //    set { LinkToPageFromLocation.NavigateUrl = value; }
        //}

        //public String MasterFlatPageLink
        //{
        //    // MasterPageLink is editable in the EditContentFlatPage.aspx.cs page.
        //    get { return LinkToPageFromFlatPage.NavigateUrl; }
        //    set { LinkToPageFromFlatPage.NavigateUrl = value; }
        //}

        //public Image MasterPageCultureFlag
        //{
        //    get { return imgCultureFlag; }
        //}

        //public DropDownList MasterPageCulture
        //{
        //    get { return cboPickCulture; }
        //}

        //public LinkButton MasterSaveContent
        //{
        //    // When the Save Content button is pressed, 
        //    // the click is resolved in the Child page.
        //    get { return SaveContent; }
        //}
    }
}
