﻿<%@ Page Title="Mobacar Leave Management" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="AddLeave.aspx.cs" Inherits="MobacarLeave._Default" %>

<%@ MasterType VirtualPath="Site.Master" %>

<%@ Register Src="~/Controls/RequestHolidays.ascx" TagName="RequestHolidays" TagPrefix="uc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    table.HolidayCalendar td a {
        display: inline-block;
        height:100%;
        width:100%;
        text-decoration: none !important;
    }
    table.HolidayCalendar td.Selectable:hover {
        background-color:Silver;
    }
    table.HolidayCalendar td.Selected {
        color:White;
        background-color:Silver;
    }
    table.HolidayCalendar td.Selected:hover {
        cursor:pointer;
    }
    table.HolidayCalendar td.Requested {
        background-color:#deff00;
    }
    table.HolidayCalendar td.PreApproved {
        background-color:#00FF21;
    }
    table.HolidayCalendar td.Approved {
        background-color:#00FF21;
    }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel 
        ID="PanelHolidayRequest" 
        runat="server" 
        DefaultButton="btnSubmitHolidayRequest"
        Style="width: 600px; margin: auto;"
        >
        <div class="row">
            <div class="col-md-6" style="padding-top: 13px;">
                <uc:RequestHolidays 
                    id="datePicker" 
                    runat="server" 
                    Style="align: center;"
                    />
            </div>
            <div class="col-md-6" style="padding-top: 13px;">
                <div class="row">
                    <asp:Label
                        ID="lblManager"
                        runat="server"
                        style="font-weight:bold;"
                        Text="Submit to Manager"
                        />
                    <br/>
                    <asp:DropDownList
                        ID="cboManagerList"
                        runat="server"
                        Class="form-control"
                        />
                </div>
                <div class="row" style="padding-top: 13px;">
                    <asp:Button 
                        ID="btnSubmitHolidayRequest" 
                        runat="server" 
                        CssClass="btn btn-primary" 
                        Style="width: 100%;"
                        Text="Submit Leave Request" 
                        UseSubmitBehavior="false" 
                        onClick="btnSubmitHolidayRequest_Click"
                        />
                </div>
                <div class="row" style="padding-top: 13px;">
                    <asp:Image ID="imgSubmittedHoliday" runat="server" imageurl="Images/SubmittedDay.png" />
                    <asp:Label ID="lblSubmittedHoliday" runat="server" Text="Submitted Leave" />
                    <br/>
                    <asp:Image ID="imgApprovedHoliday" runat="server" imageurl="Images/ApprovedDay.png" />
                    <asp:Label ID="lblApprovedHoliday" runat="server" Text="Approved Leave" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <br />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="Assets/js/bootstrap.min.js"></script>
</asp:Content>
