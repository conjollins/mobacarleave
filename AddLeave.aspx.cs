﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data;
using System.Net;
using System.Text;

public class ListHolidays
{
    public DateTime HolidayDate { get; set; }
    public char HolidayType { get; set; }       // H = Half Day; F = Full Day
    public int HolidayApproved { get; set; }    // 1 = Approved; 0 = Not Approved
}
public class ListManagers
{
    public int ManagerID { get; set; }
    public string ManagerFirstName { get; set; }
    public string ManagerFullName { get; set; }
    public string ManagerEmail { get; set; }
}

namespace MobacarLeave
{
    public partial class _Default : System.Web.UI.Page
    {
        public static List<ListManagers> ManagerList = new List<ListManagers>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Verify User; otherwise boot them to MemberLogin
            if (Session["LoggedIn"] != null)
            {
                if (!(bool)Session["LoggedIn"])
                { Response.Redirect("~/MemberLogin.aspx"); }
            }
            else { Response.Redirect("~/MemberLogin.aspx"); }

            // Update Master Header
            Master.FindControl("LogOut").Visible = true;
            Master.MasterPageLabel = "Mobacar Leave Management - Request Leave";
            Master.MasterApproveHolidays.Click += btnApproveHolidays_Click;
            Master.MasterEditEmployee.Click += btnEditEmployee_Click;
            Master.FindControl("btnReturn").Visible = false;

            // Only Managers get Approve Holiday Access
            if (Convert.ToInt32(Session["AccessLevel"]) < 3)
            {
                // Check to see if any minions have requested Holidays
                CheckMinions();

                Master.FindControl("btnEditEmployee").Visible = true;
                Master.FindControl("btnApproveHolidays").Visible = true;
            }

            // Set Calender User
            MobacarLeave.Controls.RequestHolidays.UserID = Convert.ToInt32(Session["UserID"]);

            if (!Page.IsPostBack)
            {
                // Do Stuff
                fillManagerList();

                // Reset Selected List
                MobacarLeave.Controls.RequestHolidays.listSelectedHolidays.Clear();
            }
        }

        protected void fillManagerList()
        {
            int UserID = Convert.ToInt32(Session["UserID"]);

            // Get All Managers
            DataQueries dqGetManagers = new DataQueries();
            DataTable dtManagers = dqGetManagers.GetStaff(UserID, "Managers");

            if (dtManagers != null)
            {
                cboManagerList.Items.Clear();
                for (int i = 0; dtManagers.Rows.Count > i; i++)
                {
                    ManagerList.Add(new ListManagers
                    {
                        ManagerID = Convert.ToInt32(dtManagers.Rows[i]["ID"].ToString()),
                        ManagerFirstName = dtManagers.Rows[i]["FirstName"].ToString(),
                        ManagerFullName = dtManagers.Rows[i]["FullName"].ToString(),
                        ManagerEmail = dtManagers.Rows[i]["Email"].ToString()
                    });

                    ListItem cboItm = new ListItem(
                        dtManagers.Rows[i]["FullName"].ToString(),
                        dtManagers.Rows[i]["ID"].ToString()
                        );
                    cboManagerList.Items.Add(cboItm);
                }
            }
        }

        protected void btnSubmitHolidayRequest_Click(object sender, EventArgs e)
        {
            try
            {
                // Check if Request fit within Allowances
                if (MobacarLeave.Controls.RequestHolidays.LeaveAllowance < 0)
                {
                    // Alert User
                    PopUp("Sorry, you do not have enough Annual Leave!");
                }
                else
                {
                    // Save Requests to DB; Get Holiday List from DatePick.acx.cs
                    SaveHolidays(MobacarLeave.Controls.RequestHolidays.listSelectedHolidays);

                    // Send Emails to Manager
                    SendEmails(MobacarLeave.Controls.RequestHolidays.listSelectedHolidays);

                    // Update Existing Holidays with Selected Holidays, and then clear list
                    MobacarLeave.Controls.RequestHolidays.listExistingHolidays.AddRange(MobacarLeave.Controls.RequestHolidays.listSelectedHolidays);
                    MobacarLeave.Controls.RequestHolidays.listSelectedHolidays.Clear();
                    MobacarLeave.Controls.RequestHolidays.GetExistingHolidays();        // Refresh Calendar

                    // Alert User
                    PopUp("Holiday Request Submitted to Manager.");
                }
            }
            catch (Exception ex)
            {
                // Output Error Message
            }
        }

        protected void PopUp(string Message)
        {
            Response.Write("<script>alert('" + Message + "');</script>");
        }

        protected void SaveHolidays(List<ListHolidays> SubmittedHolidays)
        {
            int EmployeeID = Convert.ToInt32(Session["UserID"]);
            string EmployeeName = Session["UserName"].ToString();
            int ManagerID = Convert.ToInt32(cboManagerList.SelectedValue);

            for (int i = 0; i < SubmittedHolidays.Count; i++)
            {
                // Collect Details
                DateTime HolidayDate = SubmittedHolidays[i].HolidayDate;
                char HolidayType = SubmittedHolidays[i].HolidayType;

                // Save Content
                DataQueries dqHoliday = new DataQueries();
                DataTable dtHoliday = dqHoliday.SaveHoliday(
                                          EmployeeID
                                        , HolidayDate
                                        , HolidayType
                                        , 0     // Not Approved
                                        , ManagerID);
            }
        }

        protected void CheckMinions()
        {
            int UserID = Convert.ToInt32(Session["UserID"]);

            // Get All Minions
            DataQueries dqGetMinonHolidayRequests = new DataQueries();
            DataTable dtMinonHolidayRequests = dqGetMinonHolidayRequests.GetStaffRequests(UserID, DateTime.Now.Year);

            string MinonHolidayRequests = dtMinonHolidayRequests.Rows[0]["StaffLeavePendingApproval"].ToString();
            
            Button myTempControl;
            myTempControl = Master.FindControl("btnApproveHolidays") as Button;
            myTempControl.Text = "Manage Staff Leave (" + MinonHolidayRequests + ")";
        }

        protected void SendEmails(List<ListHolidays> SubmittedHolidays)
        {
            try
            {
                // Get Manager Detail
                int ManagerID = Convert.ToInt32(cboManagerList.SelectedValue);
                var indexManager = FindManager(ManagerList, ManagerID);
                string ManagerEmail = ManagerList[indexManager].ManagerEmail;
                string ManagerName = ManagerList[indexManager].ManagerFirstName;

                // Get Employee Detail
                string EmployeeEmail = Session["EmailAddress"].ToString();
                string EmployeeName = Session["FirstName"].ToString() + " " + Session["LastName"].ToString();

                // Get Employee Leave Details
                double LeaveAllowance = MobacarLeave.Controls.RequestHolidays.LeaveAllowance;
                double LeavePending = MobacarLeave.Controls.RequestHolidays.LeavePending;

                // Set up Email
                string EmailFrom = "itadmin@mobacar.com";
                string EmailTo = EmployeeEmail + "," + ManagerEmail;
                string EmailSubject = "Leave Request for " + EmployeeName;

                StringBuilder EmailBody = new StringBuilder();
                EmailBody.AppendLine("<p>Hi " + ManagerName + ",</p>");
                EmailBody.AppendLine("<p>I wish to take the following holiday(s):");

                // Sort list, so Holiday Requests are in Date Order
                SubmittedHolidays.Sort((Hol1, Hol2) => DateTime.Compare(Hol1.HolidayDate, Hol2.HolidayDate));
                for (int i = 0; i < SubmittedHolidays.Count; i++)
                {
                    // Collect Details
                    string HolidayDate = SubmittedHolidays[i].HolidayDate.ToString("dddd, dd/MMM/yyyy");
                    string HolidayType = SubmittedHolidays[i].HolidayType.ToString().Replace("H", "Half").Replace("F", "Full");
                    
                    // Add Content
                    EmailBody.AppendLine("<br/>On " + HolidayDate + " for a " + HolidayType + " Day.");
                }
                EmailBody.AppendLine("</p>");

                EmailBody.AppendLine("<p>Number of Days Entitlement in current Year: " + (LeaveAllowance + LeavePending).ToString());
                EmailBody.AppendLine("<br/>Number of Days Entitlement when approved: " + LeaveAllowance.ToString() + "</p>");
                EmailBody.AppendLine("<p>Please login to <a href='http://www.mobacarleave.com/MemberLogin.aspx'>Mobacar Leave</a> to approve or disapprove these holidays.</p>");
                EmailBody.AppendLine("<p>Thanks,<br/>" + EmployeeName + "</p>");

                // Make and Send Email
                MailMessage mailObj = new MailMessage(EmailFrom, EmailTo, EmailSubject, EmailBody.ToString());
                mailObj.IsBodyHtml = true;
                SmtpClient SMTPServer = new SmtpClient("localhost");
                //SMTPServer.Credentials = new NetworkCredential("itadmin@mobacar.com", "1T_@dm!n20143");
                SMTPServer.UseDefaultCredentials = true;
                SMTPServer.Send(mailObj);
            }
            catch (Exception ex)
            {
                // Output Error Message
                string temp = ex.ToString();
                PopUp(temp);
            }
        }

        protected void btnApproveHolidays_Click(object sender, EventArgs e)
        {
            Response.Redirect("/ApproveLeave.aspx");
        }

        protected void btnEditEmployee_Click(object sender, EventArgs e)
        {
            Response.Redirect("/EditEmployee.aspx");
        }

        private int FindManager(List<ListManagers> inList, int ManagerID)
        {
            int retIndex = -1; // If not found return -1
            try
            {
                ListManagers Temp = inList.FirstOrDefault<ListManagers>(Manager => Manager.ManagerID == ManagerID);

                // If not found, Temp will be null, null has an index of -1
                retIndex = inList.IndexOf(Temp);

                return retIndex;
            }
            catch
            {
                return retIndex;
            }
        }
    }
}