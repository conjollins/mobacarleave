using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

public class DataQueries
{
    private Exception dataError;

    public Exception DataError
    {
        get { return dataError; }
    }

    public DataQueries()
    {
        dataError = null;
    }

    // MemberLogin Page Queries
    public DataTable LoginUser(String UserName, String Password)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("Leave_Login");

            SqlParameter paramUserName = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
            paramUserName.Direction = ParameterDirection.Input;
            paramUserName.Value = UserName;
            myCommand.Parameters.Add(paramUserName);

            SqlParameter paramPassword = new SqlParameter("@Password", SqlDbType.VarChar, 50);
            paramPassword.Direction = ParameterDirection.Input;
            paramPassword.Value = Password;
            myCommand.Parameters.Add(paramPassword);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Default Page Queries
    public DataTable GetExistingHolidaysByYear(int UserID, int Year)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetAllHolidaysByYear");

            SqlParameter paramUserID = new SqlParameter("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = UserID;
            myCommand.Parameters.Add(paramUserID);

            SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
            paramYear.Direction = ParameterDirection.Input;
            paramYear.Value = Year;
            myCommand.Parameters.Add(paramYear);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            //if (rows > 0)
            //{
                return returnDataTable;
            //}
            //else
            //{
            //    return null;
            //}
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveHoliday(int UserID, DateTime HolidayDate, char HolidayType, int Approved, int Manager)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_SaveHoliday");

            SqlParameter paramUserID = new SqlParameter("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = UserID;
            myCommand.Parameters.Add(paramUserID);

            SqlParameter paramHolidayDate = new SqlParameter("@inHolidayDate", SqlDbType.DateTime);
            paramHolidayDate.Direction = ParameterDirection.Input;
            paramHolidayDate.Value = HolidayDate;
            myCommand.Parameters.Add(paramHolidayDate);

            SqlParameter paramHolidayType = new SqlParameter("@inHolidayType", SqlDbType.Char);
            paramHolidayType.Direction = ParameterDirection.Input;
            paramHolidayType.Value = HolidayType;
            myCommand.Parameters.Add(paramHolidayType);

            SqlParameter paramApproved = new SqlParameter("@inHolidayApproval", SqlDbType.Int);
            paramApproved.Direction = ParameterDirection.Input;
            paramApproved.Value = Approved;
            myCommand.Parameters.Add(paramApproved);

            SqlParameter paramManager = new SqlParameter("@inManager", SqlDbType.Int);
            paramManager.Direction = ParameterDirection.Input;
            paramManager.Value = Manager;
            myCommand.Parameters.Add(paramManager);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveEmployee(int EmployeeID
                                , string FirstName
                                , string LastName
                                , string UserName
                                , string Password
                                , string EmailAddress
                                , int AccessLevel
                                , double AnnualAllowance
                                , int ReportsTo)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_SaveEmployee");

            SqlParameter paramUserID = new SqlParameter("@inEmployeeID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = EmployeeID;
            myCommand.Parameters.Add(paramUserID);

            SqlParameter paramFirstName = new SqlParameter("@inFirstName", SqlDbType.VarChar, 50);
            paramFirstName.Direction = ParameterDirection.Input;
            paramFirstName.Value = FirstName;
            myCommand.Parameters.Add(paramFirstName);

            SqlParameter paramLastName = new SqlParameter("@inLastName", SqlDbType.VarChar, 50);
            paramLastName.Direction = ParameterDirection.Input;
            paramLastName.Value = LastName;
            myCommand.Parameters.Add(paramLastName);

            SqlParameter paramUserName = new SqlParameter("@inUserName", SqlDbType.VarChar, 50);
            paramUserName.Direction = ParameterDirection.Input;
            paramUserName.Value = UserName;
            myCommand.Parameters.Add(paramUserName);

            SqlParameter paramPassword = new SqlParameter("@inPassword", SqlDbType.VarChar, 250);
            paramPassword.Direction = ParameterDirection.Input;
            paramPassword.Value = Password;
            myCommand.Parameters.Add(paramPassword);

            SqlParameter paramEmailAddress = new SqlParameter("@inEmailAddress", SqlDbType.VarChar, 250);
            paramEmailAddress.Direction = ParameterDirection.Input;
            paramEmailAddress.Value = EmailAddress;
            myCommand.Parameters.Add(paramEmailAddress);

            SqlParameter paramAccessLevel = new SqlParameter("@inAccessLevel", SqlDbType.Int);
            paramAccessLevel.Direction = ParameterDirection.Input;
            paramAccessLevel.Value = AccessLevel;
            myCommand.Parameters.Add(paramAccessLevel);

            SqlParameter paramAnnualAllowance = new SqlParameter("@inAnnualAllowance", SqlDbType.Decimal);
            paramAnnualAllowance.Direction = ParameterDirection.Input;
            paramAnnualAllowance.Value = AnnualAllowance;
            myCommand.Parameters.Add(paramAnnualAllowance);

            SqlParameter paramReportsTo = new SqlParameter("@inReportsTo", SqlDbType.Int);
            paramReportsTo.Direction = ParameterDirection.Input;
            paramReportsTo.Value = ReportsTo;
            myCommand.Parameters.Add(paramReportsTo);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Get Manager List of Minion List
    public DataTable GetLeaveTypes()
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetLeaveTypes");

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public DataTable GetEmployeeLevels(string RequestType, int UserID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetEmployeeLevels");

            SqlParameter paramRequestType = new SqlParameter("@ReqType", SqlDbType.VarChar, 20);
            paramRequestType.Direction = ParameterDirection.Input;
            paramRequestType.Value = RequestType;
            myCommand.Parameters.Add(paramRequestType);

            SqlParameter paramUserID = new SqlParameter("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = UserID;
            myCommand.Parameters.Add(paramUserID);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    // Get Manager List of Minion List
    public DataTable GetStaff(int UserID, string RequestType)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetStaff");

            SqlParameter paramUserID = new SqlParameter("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = UserID;
            myCommand.Parameters.Add(paramUserID);

            SqlParameter paramRequestType = new SqlParameter("@ReqType", SqlDbType.VarChar, 20);
            paramRequestType.Direction = ParameterDirection.Input;
            paramRequestType.Value = RequestType;
            myCommand.Parameters.Add(paramRequestType);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    // Get Leave Requests Personnal and Minions
    public DataTable GetStaffRequests(int UserID, int Year)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetStaffHolidayRequests");

            SqlParameter paramUserID = new SqlParameter("@UserID", SqlDbType.Int);
            paramUserID.Direction = ParameterDirection.Input;
            paramUserID.Value = UserID;
            myCommand.Parameters.Add(paramUserID);

            SqlParameter paramYear = new SqlParameter("@Year", SqlDbType.Int);
            paramYear.Direction = ParameterDirection.Input;
            paramYear.Value = Year;
            myCommand.Parameters.Add(paramYear);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

/*
    public DataTable GetAllFixedHolidaysByYear(int Year)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetAllFixedHolidaysByYear");

            SqlParameter paramUserName = new SqlParameter("@Year", SqlDbType.Int, 50);
            paramUserName.Direction = ParameterDirection.Input;
            paramUserName.Value = Year;
            myCommand.Parameters.Add(paramUserName);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetAllPersonalHolidaysByYear(int Year)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("sp_GetAllPersonalHolidaysByYear");

            SqlParameter paramUserName = new SqlParameter("@Year", SqlDbType.Int, 50);
            paramUserName.Direction = ParameterDirection.Input;
            paramUserName.Value = Year;
            myCommand.Parameters.Add(paramUserName);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetURLDetails(String URL)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_IdentifyContentForURL");

            SqlParameter paramURL = new SqlParameter("@inURL", SqlDbType.VarChar, 1000);
            paramURL.Direction = ParameterDirection.Input;
            paramURL.Value = URL;
            myCommand.Parameters.Add(paramURL);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable PopulateRegions()
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetRegionalisation");

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable PopulateFlatPages(String RegionID, String CountryID, String LocationID, String Culture)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetFlatPages");

            SqlParameter paramRegionID = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegionID.Direction = ParameterDirection.Input;
            paramRegionID.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegionID);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable PopulateCountries(String RegionID, String ListType)
    {
        // List Type 0: Get All Active Countries
        // List Type 1: Get Countries excluding those with Promos
        // List Type 2: Get Countries excluding those with FAQs
        // List Type 3: Get Countries excluding those with Apps
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetAllCountries");

            SqlParameter paramRegionID = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegionID.Direction = ParameterDirection.Input;
            paramRegionID.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegionID);

            SqlParameter paramListType = new SqlParameter("@inCountryListType", SqlDbType.Int);
            paramListType.Direction = ParameterDirection.Input;
            paramListType.Value = Convert.ToInt32(ListType);
            myCommand.Parameters.Add(paramListType);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable PopulateCountryChildren(int RegionID, int CountryID, String ListType)
    {
        // List Type 0: Get All Locations
        // List Type 1: Get all Country Children, including Flat Pages
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetCountryChildren");

            SqlParameter paramRegionID = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegionID.Direction = ParameterDirection.Input;
            paramRegionID.Value = RegionID;
            myCommand.Parameters.Add(paramRegionID);

            SqlParameter paramCountryID = new SqlParameter("@inCountry", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = CountryID;
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramListType = new SqlParameter("@inCountryListType", SqlDbType.Int);
            paramListType.Direction = ParameterDirection.Input;
            paramListType.Value = Convert.ToInt32(ListType);
            myCommand.Parameters.Add(paramListType);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Home Page
    public DataTable GetHomePageTiles(String RegionID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetHomePageTiles");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = RegionID;
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable getLocationTypeAhead(String TypeAheadGroup)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_DestinationTypeAhead");

            SqlParameter paramGroup = new SqlParameter("@inGroup", SqlDbType.VarChar, 20);
            paramGroup.Direction = ParameterDirection.Input;
            paramGroup.Value = TypeAheadGroup;
            myCommand.Parameters.Add(paramGroup);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveHomePageTile(String TileType, String Position, String Region, String CountryID, String LocationID, String FriendlyName)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveHomePageTile");

            SqlParameter paramTileType = new SqlParameter("@inTileType", SqlDbType.VarChar, 100);
            paramTileType.Direction = ParameterDirection.Input;
            paramTileType.Value = TileType;
            myCommand.Parameters.Add(paramTileType);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(Position);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramFriendlyName = new SqlParameter("@inFriendlyName", SqlDbType.VarChar, 50);
            paramFriendlyName.Direction = ParameterDirection.Input;
            paramFriendlyName.Value = FriendlyName;
            myCommand.Parameters.Add(paramFriendlyName);
            
            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveCompanyMessage(String Region, String CountryID, String LocationID, String Position, String Type, 
        String JobTitle, String CompanyMessage, String RepImage, String RepName)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveCompanyMessage");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(Position);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramType = new SqlParameter("@inType", SqlDbType.VarChar, 100);
            paramType.Direction = ParameterDirection.Input;
            paramType.Value = Type;
            myCommand.Parameters.Add(paramType);

            SqlParameter paramTitle = new SqlParameter("@inJobTitle", SqlDbType.VarChar, 100);
            paramTitle.Direction = ParameterDirection.Input;
            paramTitle.Value = JobTitle;
            myCommand.Parameters.Add(paramTitle);

            SqlParameter paramDescription = new SqlParameter("@inCompanyMessage", SqlDbType.VarChar, -1);
            paramDescription.Direction = ParameterDirection.Input;
            paramDescription.Value = CompanyMessage;
            myCommand.Parameters.Add(paramDescription);

            SqlParameter paramURL = new SqlParameter("@inRepImage", SqlDbType.VarChar, 100);
            paramURL.Direction = ParameterDirection.Input;
            paramURL.Value = RepImage;
            myCommand.Parameters.Add(paramURL);

            SqlParameter paramImageAlt = new SqlParameter("@inRepName", SqlDbType.VarChar, 100);
            paramImageAlt.Direction = ParameterDirection.Input;
            paramImageAlt.Value = RepName;
            myCommand.Parameters.Add(paramImageAlt);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Country & Location Pages
    public DataTable GetMetaTiles(String RegionID, String CountryID, String LocationID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetCountryLocationMeta");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetPopularLocations(String RegionID, String CountryID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetCountryPopularLocations");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveCountryLocationTile(String Region, String CountryID, String LocationID, String Position, String MetaValue)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveCountryLocationMeta");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(Position);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramMetaValue = new SqlParameter("@inMetaValue", SqlDbType.VarChar, 1000);
            paramMetaValue.Direction = ParameterDirection.Input;
            paramMetaValue.Value = MetaValue;
            myCommand.Parameters.Add(paramMetaValue);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveCountryPopularLocation(String Region, String CountryID, String Position, String PopularLocationID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveCountryPopularLocation");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(Position);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramLocationID = new SqlParameter("@inPopularLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(PopularLocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Location
    public DataTable GenerateCarTypeMatrixContent(String RegionID, String CarType)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GenerateCarTypeMatrix");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCarType = new SqlParameter("@inCarType", SqlDbType.VarChar, 50);
            paramCarType.Direction = ParameterDirection.Input;
            paramCarType.Value = CarType;
            myCommand.Parameters.Add(paramCarType);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetCarClasses()
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetCarClass");
            
            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Promo Codes
    public DataTable GetPromoCodes(String RegionID, String PromoID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetPromoCode");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramPromo = new SqlParameter("@inPromoID", SqlDbType.VarChar, 2);
            paramPromo.Direction = ParameterDirection.Input;
            paramPromo.Value = Convert.ToInt32(PromoID);
            myCommand.Parameters.Add(paramPromo);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SavePromoCodes(String RegionID, String PromoID, String PromoCode, String PromoCountry, String PromoSaving, String PromoExpiry, String PromoActive)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SavePromoCode");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramPromoID = new SqlParameter("@inPromoID", SqlDbType.Int);
            paramPromoID.Direction = ParameterDirection.Input;
            paramPromoID.Value = Convert.ToInt32(PromoID);
            myCommand.Parameters.Add(paramPromoID);

            SqlParameter paramPromoCode = new SqlParameter("@inPromoCode", SqlDbType.VarChar, 50);
            paramPromoCode.Direction = ParameterDirection.Input;
            paramPromoCode.Value = PromoCode;
            myCommand.Parameters.Add(paramPromoCode);

            SqlParameter paramPromoCountry = new SqlParameter("@inPromoCountry", SqlDbType.Int);
            paramPromoCountry.Direction = ParameterDirection.Input;
            paramPromoCountry.Value = Convert.ToInt32(PromoCountry);
            myCommand.Parameters.Add(paramPromoCountry);

            SqlParameter paramPromoContent = new SqlParameter("@inPromoSaving", SqlDbType.Int);
            paramPromoContent.Direction = ParameterDirection.Input;
            paramPromoContent.Value = Convert.ToInt32(PromoSaving);
            myCommand.Parameters.Add(paramPromoContent);

            SqlParameter paramPromoExpiry = new SqlParameter("@inPromoExpiry", SqlDbType.DateTime);
            paramPromoExpiry.Direction = ParameterDirection.Input;
            paramPromoExpiry.Value = PromoExpiry;
            myCommand.Parameters.Add(paramPromoExpiry);

            SqlParameter paramPromoActive = new SqlParameter("@inPromoActive", SqlDbType.Int);
            paramPromoActive.Direction = ParameterDirection.Input;
            paramPromoActive.Value = Convert.ToInt32(PromoActive);
            myCommand.Parameters.Add(paramPromoActive);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Apps
    public DataTable GetApps(String RegionID, String CountryID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetApps");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountry = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountry.Direction = ParameterDirection.Input;
            paramCountry.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountry);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveApp(String Region, String Country, String Order, String AppName, String AppDesc, String LinkAndroid, String LinkiOS, String ImageURL)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveApp");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountry = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountry.Direction = ParameterDirection.Input;
            paramCountry.Value = Convert.ToInt32(Country);
            myCommand.Parameters.Add(paramCountry);

            SqlParameter paramOrder = new SqlParameter("@inOrder", SqlDbType.Int);
            paramOrder.Direction = ParameterDirection.Input;
            paramOrder.Value = Convert.ToInt32(Order);
            myCommand.Parameters.Add(paramOrder);

            SqlParameter paramAppName = new SqlParameter("@inAppName", SqlDbType.VarChar, 100);
            paramAppName.Direction = ParameterDirection.Input;
            paramAppName.Value = AppName;
            myCommand.Parameters.Add(paramAppName);

            SqlParameter paramAppDesc = new SqlParameter("@inAppDesc", SqlDbType.VarChar, -1);
            paramAppDesc.Direction = ParameterDirection.Input;
            paramAppDesc.Value = AppDesc;
            myCommand.Parameters.Add(paramAppDesc);

            SqlParameter paramLinkAndroid = new SqlParameter("@inLinkAndroid", SqlDbType.VarChar, 500);
            paramLinkAndroid.Direction = ParameterDirection.Input;
            paramLinkAndroid.Value = LinkAndroid;
            myCommand.Parameters.Add(paramLinkAndroid);

            SqlParameter paramLinkiOS = new SqlParameter("@inLinkiOS", SqlDbType.VarChar, 500);
            paramLinkiOS.Direction = ParameterDirection.Input;
            paramLinkiOS.Value = LinkiOS;
            myCommand.Parameters.Add(paramLinkiOS);

            SqlParameter paramImage = new SqlParameter("@inImageURL", SqlDbType.VarChar, 500);
            paramImage.Direction = ParameterDirection.Input;
            paramImage.Value = ImageURL;
            myCommand.Parameters.Add(paramImage);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Reviews
    public DataTable GetReviewList(String RegionID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetReviewList");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetReview(String RegionID, String ReviewID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetReview");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramReview = new SqlParameter("@inReviewID", SqlDbType.Int);
            paramReview.Direction = ParameterDirection.Input;
            paramReview.Value = Convert.ToInt32(ReviewID);
            myCommand.Parameters.Add(paramReview);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable DeleteReview(String RegionID, String ReviewID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_DeleteReview");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramReview = new SqlParameter("@inReviewID", SqlDbType.Int);
            paramReview.Direction = ParameterDirection.Input;
            paramReview.Value = Convert.ToInt32(ReviewID);
            myCommand.Parameters.Add(paramReview);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetLocations(String CountryID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetLocations");
            
            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveReview(String RegionID, String ReviewID
                                , String CustomerName, String RentalDate, String CustomerCountry
                                , String BookingReference, String DestCountry, String DestLocation
                                , String ReviewContent)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveReview");

            SqlParameter paramRegion = new SqlParameter("@RegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramReviewID = new SqlParameter("@ReviewID", SqlDbType.Int);
            paramReviewID.Direction = ParameterDirection.Input;
            paramReviewID.Value = Convert.ToInt32(ReviewID);
            myCommand.Parameters.Add(paramReviewID);


            SqlParameter paramCustomerName = new SqlParameter("@CustomerName", SqlDbType.VarChar, 100);
            paramCustomerName.Direction = ParameterDirection.Input;
            paramCustomerName.Value = CustomerName;
            myCommand.Parameters.Add(paramCustomerName);

            SqlParameter paramRentalDate = new SqlParameter("@RentalDate", SqlDbType.DateTime);
            paramRentalDate.Direction = ParameterDirection.Input;
            paramRentalDate.Value = RentalDate;
            myCommand.Parameters.Add(paramRentalDate);

            SqlParameter paramCustomerCountry = new SqlParameter("@CustomerCountry", SqlDbType.Int);
            paramCustomerCountry.Direction = ParameterDirection.Input;
            paramCustomerCountry.Value = Convert.ToInt32(CustomerCountry);
            myCommand.Parameters.Add(paramCustomerCountry);


            SqlParameter paramBookingReference = new SqlParameter("@BookingReference", SqlDbType.VarChar, 50);
            paramBookingReference.Direction = ParameterDirection.Input;
            paramBookingReference.Value = BookingReference;
            myCommand.Parameters.Add(paramBookingReference);

            SqlParameter paramDestCountry = new SqlParameter("@PickupCountry", SqlDbType.Int);
            paramDestCountry.Direction = ParameterDirection.Input;
            paramDestCountry.Value = Convert.ToInt32(DestCountry);
            myCommand.Parameters.Add(paramDestCountry);

            SqlParameter paramPickupLocation = new SqlParameter("@PickupLocation", SqlDbType.Int);
            paramPickupLocation.Direction = ParameterDirection.Input;
            paramPickupLocation.Value = Convert.ToInt32(DestLocation);
            myCommand.Parameters.Add(paramPickupLocation);


            SqlParameter paramReviewContent = new SqlParameter("@Content", SqlDbType.VarChar, -1);
            paramReviewContent.Direction = ParameterDirection.Input;
            paramReviewContent.Value = ReviewContent;
            myCommand.Parameters.Add(paramReviewContent);


            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit FAQs
    public DataTable GetFAQList(String RegionID, String Culture)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetFAQList");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetFAQ(String RegionID, String CountryID, String LocationID, String Culture)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetFAQ");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountry = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountry.Direction = ParameterDirection.Input;
            paramCountry.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountry);

            SqlParameter paramLocation = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocation.Direction = ParameterDirection.Input;
            paramLocation.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocation);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveFAQ(String Region, String Country, String Location, String Order, String Question, String Answer, String Culture)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveFAQ");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountry = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountry.Direction = ParameterDirection.Input;
            paramCountry.Value = Convert.ToInt32(Country);
            myCommand.Parameters.Add(paramCountry);

            SqlParameter paramLocation = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocation.Direction = ParameterDirection.Input;
            paramLocation.Value = Convert.ToInt32(Location);
            myCommand.Parameters.Add(paramLocation);

            SqlParameter paramOrder = new SqlParameter("@inOrder", SqlDbType.Int);
            paramOrder.Direction = ParameterDirection.Input;
            paramOrder.Value = Convert.ToInt32(Order);
            myCommand.Parameters.Add(paramOrder);

            SqlParameter paramQuestion = new SqlParameter("@inQuestion", SqlDbType.VarChar, 150);
            paramQuestion.Direction = ParameterDirection.Input;
            paramQuestion.Value = Question;
            myCommand.Parameters.Add(paramQuestion);

            SqlParameter paramAnswer = new SqlParameter("@inAnswer", SqlDbType.VarChar, -1);
            paramAnswer.Direction = ParameterDirection.Input;
            paramAnswer.Value = Answer;
            myCommand.Parameters.Add(paramAnswer);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 50);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Location Matrix
    public DataTable GetLocationMatrix(String RegionID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetMatrixLocation");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveLocationMatrix(String Region, String Line, String Variant, String Content)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveMatrixLocation");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramLine = new SqlParameter("@inLine", SqlDbType.Int);
            paramLine.Direction = ParameterDirection.Input;
            paramLine.Value = Convert.ToInt32(Line);
            myCommand.Parameters.Add(paramLine);

            SqlParameter paramVariant = new SqlParameter("@inVariant", SqlDbType.Int);
            paramVariant.Direction = ParameterDirection.Input;
            paramVariant.Value = Convert.ToInt32(Variant);
            myCommand.Parameters.Add(paramVariant);

            SqlParameter paramDescription = new SqlParameter("@inContent", SqlDbType.VarChar, -1);
            paramDescription.Direction = ParameterDirection.Input;
            paramDescription.Value = Content;
            myCommand.Parameters.Add(paramDescription);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 10);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "EN";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Edit Car Type Matrix
    public DataTable GetCarTypeMatrix(String RegionID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetMatrixCarTypes");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveCarTypeMatrix(String Region, int CarClass, String CarType, String Line, String Variant, String Content)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveMatrixCarTypes");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCarClass = new SqlParameter("@inCarClass", SqlDbType.VarChar, 50);
            paramCarClass.Direction = ParameterDirection.Input;
            paramCarClass.Value = CarClass;
            myCommand.Parameters.Add(paramCarClass);

            SqlParameter paramCarType = new SqlParameter("@inCarType", SqlDbType.VarChar, 50);
            paramCarType.Direction = ParameterDirection.Input;
            paramCarType.Value = CarType;
            myCommand.Parameters.Add(paramCarType);

            SqlParameter paramLine = new SqlParameter("@inLine", SqlDbType.Int);
            paramLine.Direction = ParameterDirection.Input;
            paramLine.Value = Convert.ToInt32(Line);
            myCommand.Parameters.Add(paramLine);

            SqlParameter paramVariant = new SqlParameter("@inVariant", SqlDbType.Int);
            paramVariant.Direction = ParameterDirection.Input;
            paramVariant.Value = Convert.ToInt32(Variant);
            myCommand.Parameters.Add(paramVariant);

            SqlParameter paramDescription = new SqlParameter("@inContent", SqlDbType.VarChar, -1);
            paramDescription.Direction = ParameterDirection.Input;
            paramDescription.Value = Content;
            myCommand.Parameters.Add(paramDescription);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 10);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "EN";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Get Region Settings
    public DataTable GetRegionSettings(String RegionID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetRegionSettings");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    // Multiple Pages
    public DataTable GetContent(String PageType, String RegionID, String ContentType, String CountryID, String LocationID, String Culture)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetContent");

            SqlParameter paramPageType = new SqlParameter("@inPageType", SqlDbType.VarChar, 50);
            paramPageType.Direction = ParameterDirection.Input;
            paramPageType.Value = PageType;
            myCommand.Parameters.Add(paramPageType);

            SqlParameter paramRegionID = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegionID.Direction = ParameterDirection.Input;
            paramRegionID.Value = RegionID;
            myCommand.Parameters.Add(paramRegionID);

            SqlParameter paramContentType = new SqlParameter("@inContentTypeID", SqlDbType.Int);
            paramContentType.Direction = ParameterDirection.Input;
            paramContentType.Value = ContentType;
            myCommand.Parameters.Add(paramContentType);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = CountryID;
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = LocationID;
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetMiniContent(String RegionID, String CountryID, String LocationID, String Type)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetCountryLocationMini");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramType = new SqlParameter("@inType", SqlDbType.VarChar, 100);
            paramType.Direction = ParameterDirection.Input;
            paramType.Value = Type;
            myCommand.Parameters.Add(paramType);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 2);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = "en";
            myCommand.Parameters.Add(paramCulture);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    
    public DataTable SaveContent(String RegionID, String ContentTypeID, String CountryID, String LocationID, String Culture,
        String MetaTitle, String MetaKeywords, String MetaDescription, String MetaIndex,
        String MainContent, String SecondContent,
        int UserID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveContent");

            SqlParameter paramRegionID = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegionID.Direction = ParameterDirection.Input;
            paramRegionID.Value = Convert.ToInt32(RegionID);
            myCommand.Parameters.Add(paramRegionID);

            SqlParameter paramContentTypeID = new SqlParameter("@inContentTypeID", SqlDbType.Int);
            paramContentTypeID.Direction = ParameterDirection.Input;
            paramContentTypeID.Value = Convert.ToInt32(ContentTypeID);
            myCommand.Parameters.Add(paramContentTypeID);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramCulture = new SqlParameter("@inCulture", SqlDbType.VarChar, 10);
            paramCulture.Direction = ParameterDirection.Input;
            paramCulture.Value = Culture;
            myCommand.Parameters.Add(paramCulture);


            SqlParameter paramMetaTitle = new SqlParameter("@inMetaTitle", SqlDbType.NVarChar, 1000);
            paramMetaTitle.Direction = ParameterDirection.Input;
            paramMetaTitle.Value = MetaTitle;
            myCommand.Parameters.Add(paramMetaTitle);

            SqlParameter paramMetaKeywords = new SqlParameter("@inMetaKeywords", SqlDbType.NVarChar, 2000);
            paramMetaKeywords.Direction = ParameterDirection.Input;
            paramMetaKeywords.Value = MetaKeywords;
            myCommand.Parameters.Add(paramMetaKeywords);

            SqlParameter paramMetaDescription = new SqlParameter("@inMetaDescription", SqlDbType.NVarChar, 2000);
            paramMetaDescription.Direction = ParameterDirection.Input;
            paramMetaDescription.Value = MetaDescription;
            myCommand.Parameters.Add(paramMetaDescription);

            SqlParameter paramMetaIndex = new SqlParameter("@inMetaIndex", SqlDbType.Int);
            paramMetaIndex.Direction = ParameterDirection.Input;
            paramMetaIndex.Value = MetaIndex;
            myCommand.Parameters.Add(paramMetaIndex);
            

            SqlParameter paramMainContent = new SqlParameter("@inMainContent", SqlDbType.NVarChar, -1);
            paramMainContent.Direction = ParameterDirection.Input;
            paramMainContent.Value = MainContent;
            myCommand.Parameters.Add(paramMainContent);

            SqlParameter paramIntro1 = new SqlParameter("@inSecondContent", SqlDbType.NVarChar, -1);
            paramIntro1.Direction = ParameterDirection.Input;
            paramIntro1.Value = SecondContent;
            myCommand.Parameters.Add(paramIntro1);

            SqlParameter paramUser = new SqlParameter("@inUserID", SqlDbType.Int);
            paramUser.Direction = ParameterDirection.Input;
            paramUser.Value = UserID;
            myCommand.Parameters.Add(paramUser);


            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveMiniContent(String Region, String CountryID, String LocationID, String Position, String Type, String Title, String Content, String URL, String ImageAlt)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveCountryLocationMini");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(Position);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramType = new SqlParameter("@inType", SqlDbType.VarChar, 100);
            paramType.Direction = ParameterDirection.Input;
            paramType.Value = Type;
            myCommand.Parameters.Add(paramType);

            SqlParameter paramTitle = new SqlParameter("@inTitle", SqlDbType.VarChar, 100);
            paramTitle.Direction = ParameterDirection.Input;
            paramTitle.Value = Title;
            myCommand.Parameters.Add(paramTitle);

            SqlParameter paramDescription = new SqlParameter("@inContent", SqlDbType.VarChar, -1);
            paramDescription.Direction = ParameterDirection.Input;
            paramDescription.Value = Content;
            myCommand.Parameters.Add(paramDescription);

            SqlParameter paramURL = new SqlParameter("@inURL", SqlDbType.VarChar, 100);
            paramURL.Direction = ParameterDirection.Input;
            paramURL.Value = URL;
            myCommand.Parameters.Add(paramURL);

            SqlParameter paramImageAlt = new SqlParameter("@inImageAlt", SqlDbType.VarChar, 100);
            paramImageAlt.Direction = ParameterDirection.Input;
            paramImageAlt.Value = ImageAlt;
            myCommand.Parameters.Add(paramImageAlt);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable GetImages(String Region, String CountryID, String LocationID)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_GetImages");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public DataTable SaveImage(String Region, String CountryID, String LocationID, String ImageType, String ImagePosition, String ImageURL, String ImageText)
    {
        try
        {
            DatabaseConnection DataBaseConnection = new DatabaseConnection();
            SqlConnection connection = DataBaseConnection.GetConnection();
            SqlCommand myCommand = DataBaseConnection.GetCommandForSP("CMS2_SaveImage");

            SqlParameter paramRegion = new SqlParameter("@inRegionID", SqlDbType.Int);
            paramRegion.Direction = ParameterDirection.Input;
            paramRegion.Value = Convert.ToInt32(Region);
            myCommand.Parameters.Add(paramRegion);

            SqlParameter paramCountryID = new SqlParameter("@inCountryID", SqlDbType.Int);
            paramCountryID.Direction = ParameterDirection.Input;
            paramCountryID.Value = Convert.ToInt32(CountryID);
            myCommand.Parameters.Add(paramCountryID);

            SqlParameter paramLocationID = new SqlParameter("@inLocationID", SqlDbType.Int);
            paramLocationID.Direction = ParameterDirection.Input;
            paramLocationID.Value = Convert.ToInt32(LocationID);
            myCommand.Parameters.Add(paramLocationID);

            SqlParameter paramType = new SqlParameter("@inImageType", SqlDbType.Int);
            paramType.Direction = ParameterDirection.Input;
            paramType.Value = Convert.ToInt32(ImageType);
            myCommand.Parameters.Add(paramType);

            SqlParameter paramPosition = new SqlParameter("@inPosition", SqlDbType.Int);
            paramPosition.Direction = ParameterDirection.Input;
            paramPosition.Value = Convert.ToInt32(ImagePosition);
            myCommand.Parameters.Add(paramPosition);

            SqlParameter paramURL = new SqlParameter("@inImageURL", SqlDbType.VarChar, 500);
            paramURL.Direction = ParameterDirection.Input;
            paramURL.Value = ImageURL;
            myCommand.Parameters.Add(paramURL);

            SqlParameter paramImageAlt = new SqlParameter("@inImageText", SqlDbType.VarChar, 1000);
            paramImageAlt.Direction = ParameterDirection.Input;
            paramImageAlt.Value = ImageText;
            myCommand.Parameters.Add(paramImageAlt);

            SqlDataAdapter adap = new SqlDataAdapter(myCommand);
            DataTable returnDataTable = new DataTable();
            adap.Fill(returnDataTable);
            int rows = returnDataTable.Rows.Count;

            connection.Close();

            if (rows > 0)
            {
                return returnDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
*/
}