using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Summary description for DatabaseConnection
/// </summary>
public class DatabaseConnection
{
	public DatabaseConnection()
	{
		// TODO: Add constructor logic here
	}

    int cnCountTotal;
    int cnCurrent;
    
    /// Creates and returns a sql connection object. The connection string is retrieved using Section handler defined by the application configuration file.
    /// 
    /// <returns>An Open SqlConnection</returns>
    public SqlConnection connection;

    public SqlConnection GetConnection()
    {
        if (connection == null)
        {
            connection = new SqlConnection(ConfigurationManager.AppSettings["MobaDb"]);
            connection.Open();
            cnCountTotal++;
            cnCurrent = cnCountTotal;
        }

        return connection;
    }

    /// Creates a sql command. It uses an existing connection object or creates a new connection object.
    /// 
    /// <param name="sqlString">Sql query to execute</param>
    /// <returns>Command object</returns>
    public SqlCommand GetCommand(string sqlString)
    {
        SqlCommand command = new SqlCommand(sqlString, GetConnection());
        return command;
    }

    /// Creates a sql command. It uses an existing connection object or creates a new connection object.
    /// <param name="spName">Name of the stored procedure or function to be executed</param>
    /// <returns>Command object</returns>
    public SqlCommand GetCommandForSP(string spName)
    {
        SqlCommand cmd = GetCommand(spName);
        cmd.CommandType = CommandType.StoredProcedure;
        return cmd;
    }

    
}
