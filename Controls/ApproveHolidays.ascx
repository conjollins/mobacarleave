﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApproveHolidays.ascx.cs" Inherits="MobacarLeave.Controls.ApproveHolidays" %>

<asp:ScriptManager ID="ScriptManager1" runat="server" />
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="row">
            <div class="col-md-12">
                <asp:Calendar 
                    ID="Calendar1" 
                    Class="HolidayCalendar"
                    runat="server" 
                    onselectionchanged="Calendar1_SelectionChanged" 
                    ViewStateMode="Enabled" 
                    OnDayRender="DayRender"
                    SelectionMode="DayWeekMonth"/>
            </div>
        </div>
        <div class="row" style="padding-top: 10px;">
            <div class="col-md-6">
                <asp:Label ID="lblAnnualAllowance" runat="server" Text="Annual Allowance" />
            </div>
            <div class="col-md-6">
                <asp:Label ID="disAnnualAllowance" runat="server" Text="" />
            </div>
        </div>
        <div class="row" style="padding-top: 5px;">
            <div class="col-md-6">
                <asp:Label ID="lblAllowanceRemaining" runat="server" Text="Leave Remaining" />
            </div>
            <div class="col-md-6">
                <asp:Label ID="disAllowanceRemaining" runat="server" Text="" />
            </div>
        </div>
        <div class="row" style="padding-top: 5px;">
            <div class="col-md-6">
                <asp:Label ID="lblAllowancePending" runat="server" Text="Leave Pending" />
            </div>
            <div class="col-md-6">
                <asp:Label ID="disAllowancePending" runat="server" Text=""  />
            </div>
        </div>
    </ContentTemplate> 
</asp:UpdatePanel>