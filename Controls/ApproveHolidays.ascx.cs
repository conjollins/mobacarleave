﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace MobacarLeave.Controls
{
    public partial class ApproveHolidays : System.Web.UI.UserControl
    {
        public static int UserID { get; set; }
        public static double AnnualAllowance { get; set; }
        public static double LeaveRemaining { get; set; }
        public static double LeavePending { get; set; }
        public static string LeaveType { get; set; }
        public static List<ListHolidayRequests> listRequestedHolidays = new List<ListHolidayRequests>();
        public static List<ListHolidayRequests> listApprovedLeave = new List<ListHolidayRequests>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Ensure Approved list is empty
                listApprovedLeave.Clear();
            }

            GetRequestedHolidays();
        }

        public static void GetRequestedHolidays()
        {
            listRequestedHolidays.Clear();

            // Output list of Public Holidays
            DataQueries dqHolidays = new DataQueries();
            DataTable dtHolidays = dqHolidays.GetExistingHolidaysByYear(UserID, DateTime.Now.Year);

            foreach (DataRow dr in dtHolidays.Rows)
            {
                listRequestedHolidays.Add(new ListHolidayRequests
                {
                    HolidayDate = (DateTime)dr["LeaveDate"],
                    HolidayType = dr["LeaveType"].ToString()[0],
                    HolidayApproved = Convert.ToInt32(dr["LeaveApproved"])
                });
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // These are set from Parent and Updated in ApproveHoliday/AddHoliday
            UpdateLabels();
        }
        
        protected void UpdateLabels()
        {
            // These are set from Parent and Updated in AdvanceDate
            if (UserID == 0)
            {
                AnnualAllowance = 0;
                LeaveRemaining = 0;
                LeavePending = 0;

                lblAnnualAllowance.Visible = false;
                disAnnualAllowance.Visible = false;
                lblAllowanceRemaining.Visible = false;
                disAllowanceRemaining.Visible = false;
                lblAllowancePending.Visible = false;
                disAllowancePending.Visible = false;
            }
            else{
                disAnnualAllowance.Text = AnnualAllowance.ToString();
                disAllowanceRemaining.Text = LeaveRemaining.ToString();
                disAllowancePending.Text = LeavePending.ToString();

                lblAnnualAllowance.Visible = true;
                disAnnualAllowance.Visible = true;
                lblAllowanceRemaining.Visible = true;
                disAllowanceRemaining.Visible = true;
                lblAllowancePending.Visible = true;
                disAllowancePending.Visible = true;
            }
        }

        protected void DayRender(object sender, DayRenderEventArgs e)
        {
            // Day Render cycles through all days of displayed Calendar 
            // and checks each one to see if it is a Requested Holiday
            CalendarDay RenderingDay = e.Day;
            TableCell RenderingCell = e.Cell;
            
            // Disallow Weekends
            if (RenderingDay.IsWeekend)
            {
                RenderingDay.IsSelectable = false;
            }
            // Everything else is Selectable by default
            else
            {
                RenderingCell.CssClass = "Selectable";
            }
            
            // Display Requested Holidays
            int indexRequestedHols = FindDate(listRequestedHolidays, RenderingDay.Date);
            if (indexRequestedHols >= 0)
            {
                RenderingCell.Text = listRequestedHolidays[indexRequestedHols].HolidayType.ToString();

                // If the holidays are Company or Approved from a previous time
                if (listRequestedHolidays[indexRequestedHols].HolidayType.ToString() == "C"
                    ||
                    listRequestedHolidays[indexRequestedHols].HolidayApproved == 1)
                {
                    RenderingDay.IsSelectable = false;
                    RenderingCell.CssClass = "PreApproved";
                }
                // New requests
                else if (listRequestedHolidays[indexRequestedHols].HolidayApproved == 0)
                {
                    // Normal Holidays Days
                    RenderingDay.IsSelectable = true;
                    RenderingCell.Attributes.Add("OnClick", e.SelectUrl);
                    RenderingCell.CssClass = "Requested";
                }
                // Denied requests
                //else if (listRequestedHolidays[indexRequestedHols].HolidayApproved == -1)
                //{
                    // DisApproved Holidays
                    //RenderingDay.IsSelectable = true;
                    //RenderingCell.Attributes.Add("OnClick", e.SelectUrl);
                    //RenderingCell.CssClass = "DisApproved";
                //}
            }

            // Change Requested Holiday to Approved Holiday
            int indexApprovedHols = FindDate(listApprovedLeave, RenderingDay.Date);
            if (indexApprovedHols >= 0)
            {
                RenderingDay.IsSelectable = true;
                RenderingCell.Attributes.Add("OnClick", e.SelectUrl);
                if (listApprovedLeave[indexApprovedHols].HolidayType.ToString() == "D")
                {
                    RenderingCell.CssClass = "DisApproved";
                }
                else
                {
                    RenderingCell.CssClass = "Approved";
                }
                RenderingCell.Text = listApprovedLeave[indexApprovedHols].HolidayType.ToString();
            }
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            if (UserID != 0 && LeaveType != null)
            {
                // Loop Through all Dates selected
                for (int i = 0; i < Calendar1.SelectedDates.Count; i++)
                {
                    DateTime DateToCheck = Calendar1.SelectedDates[i];

                    // Weekends aren't normally selectable but selecting a week or month does so
                    if (!isWeekend(DateToCheck))
                    {
                        // Is Clicked Date a Requested Holiday
                        int indexRequestedHols = FindDate(listRequestedHolidays, DateToCheck);

                        // If Clicked Date is a Requested Leave
                        if (indexRequestedHols >= 0)
                        {
                            int HolidayApproved = listRequestedHolidays[indexRequestedHols].HolidayApproved;

                            // If LeaveType is Holiday and Clicked Date is a Requested Leave (and as yet unapproved); then Approve Leave
                            if (HolidayApproved != 1 && LeaveType == "0")
                            {
                                ApproveHoliday(DateToCheck, listRequestedHolidays[indexRequestedHols].HolidayType);
                            }
                        }
                        else if (indexRequestedHols < 0)
                        {
                            // If LeaveType is Holiday and Clicked Date is NOT a Requested Holiday; Add and Approve
                            if (LeaveType == "0")
                            {
                                AddHoliday(DateToCheck);
                            }
                            // If LeaveType is Sick Leave, etc. and Clicked Date is NOT a Requested Holiday; Add and Approve
                            else if (LeaveType != "0")
                            {
                                AddLeave(DateToCheck);
                            }
                        }
                    }
                }
            }

            // Clearing Selected Dates allows you to click on same date twice
            Calendar1.SelectedDates.Clear();
        }

        private void ApproveHoliday(DateTime ClickedDay, char RequestedHolidayType)
        {
            var index = FindDate(listApprovedLeave, ClickedDay);

            // Move Approved to DisApproved; or DisApproved to Approved
            if (index >= 0)
            {
                // Move Approved to DisApproved
                if (listApprovedLeave[index].HolidayType == 'F')
                {
                    LeaveRemaining = LeaveRemaining + 1;
                    listApprovedLeave[index].HolidayApproved = -1;
                    listApprovedLeave[index].HolidayType = 'D';
                }
                else if (listApprovedLeave[index].HolidayType == 'H')
                {
                    LeaveRemaining = LeaveRemaining + 0.5;
                    listApprovedLeave[index].HolidayApproved = -1;
                    listApprovedLeave[index].HolidayType = 'D';
                }
                // Move DisApproved to Approved
                else if (listApprovedLeave[index].HolidayType == 'D')
                {
                    // Approve Holiday
                    listApprovedLeave[index].HolidayApproved = 1;
                    listApprovedLeave[index].HolidayType = RequestedHolidayType;

                    if (RequestedHolidayType == 'F')
                    {
                        LeaveRemaining = LeaveRemaining - 1;
                    }
                    else if (RequestedHolidayType == 'H')
                    {
                        LeaveRemaining = LeaveRemaining - 0.5;
                    }
                }
            }
            // Move UnApproved to Approved
            else if (index < 0)
            {
                listApprovedLeave.Add(new ListHolidayRequests
                {
                    HolidayDate = ClickedDay,
                    HolidayType = RequestedHolidayType,
                    HolidayApproved = 1
                });

                if (RequestedHolidayType == 'F')
                {
                    LeaveRemaining = LeaveRemaining - 1;
                    LeavePending = LeavePending - 1;
                }
                else if (RequestedHolidayType == 'H')
                {
                    LeaveRemaining = LeaveRemaining - 0.5;
                    LeavePending = LeavePending - 0.5;
                }
            }
        }

        private void AddHoliday(DateTime ClickedDay)
        {
            // First Click is Auto Approved Half Day
            var index = FindDate(listApprovedLeave, ClickedDay);
            if (index < 0)
            {
                listApprovedLeave.Add(new ListHolidayRequests
                {
                    HolidayDate = ClickedDay,
                    HolidayType = 'H',
                    HolidayApproved = 1
                });

                LeaveRemaining = LeaveRemaining - 0.5;
            }
            // Second Click is Auto Removed
            else
            {
                if (listApprovedLeave[index].HolidayType == 'H')
                {
                    listApprovedLeave[index].HolidayType = 'F';

                    LeaveRemaining = LeaveRemaining - 0.5;
                }
                else if (listApprovedLeave[index].HolidayType == 'F')
                {
                    listApprovedLeave.RemoveAt(index);

                    LeaveRemaining = LeaveRemaining + 1;
                }
            }
        }

        private void AddLeave(DateTime ClickedDay)
        {
            // First Click is Auto Approved
            var index = FindDate(listApprovedLeave, ClickedDay);
            if (index < 0)
            {
                listApprovedLeave.Add(new ListHolidayRequests
                {
                    HolidayDate = ClickedDay,
                    HolidayType = Convert.ToChar(LeaveType),
                    HolidayApproved = 1
                });
            }
            // Second Click is Auto Removed
            else
            {
                listApprovedLeave.RemoveAt(index);
            }
        }

        public static bool isWeekend(DateTime dtToValidate)
        {
            if (dtToValidate.DayOfWeek == DayOfWeek.Sunday || dtToValidate.DayOfWeek == DayOfWeek.Saturday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int FindDate(List<ListHolidayRequests> inList, DateTime Day)
        {
            int retIndex = -1; // If not found return -1
            try
            {
                ListHolidayRequests Temp = inList.FirstOrDefault<ListHolidayRequests>(Holiday => Holiday.HolidayDate == Day);

                // If not found, Temp will be null, null has an index of -1
                retIndex = inList.IndexOf(Temp);

                return retIndex;
            }
            catch {
                return retIndex;
            }
        }
    }
}