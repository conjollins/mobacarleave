﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace MobacarLeave.Controls
{
    public partial class RequestHolidays : System.Web.UI.UserControl
    {
        public static int UserID { get; set; }
        public static double LeaveAllowance { get; set; }
        public static double LeavePending { get; set; }
        public static List<ListHolidays> listSelectedHolidays = new List<ListHolidays>();
        public static List<ListHolidays> listExistingHolidays = new List<ListHolidays>();

        protected void Page_Load(object sender, EventArgs e)
        {
            GetExistingHolidays();

            if (!Page.IsPostBack)
            {
                // Update Annual Leave Labels.
                UpdateAllowances();
            }
        }

        protected void DayRender(object sender, DayRenderEventArgs e)
        {
            // Day Render cycles through all days of displayed Calendar 
            // and checks each one to see if it is: in the past, a weekend, 
            // or in the lists Existing Holidays or Selected Holidays
            CalendarDay RenderingDay = e.Day;
            TableCell RenderingCell = e.Cell;

            // Disallow Days in Past & Weekends
            if ((DateTime.Compare(DateTime.Today, RenderingDay.Date) > 0) || (RenderingDay.IsWeekend))
            {
                RenderingDay.IsSelectable = false;
            }
            else
            {
                RenderingCell.CssClass = "Selectable";
            }

            // Disallow Existing Holidays
            int indexExistingHols = FindDate(listExistingHolidays, RenderingDay.Date);
            if (indexExistingHols >= 0)
            {
                RenderingDay.IsSelectable = false;
                RenderingCell.Text = listExistingHolidays[indexExistingHols].HolidayType.ToString();

                if (listExistingHolidays[indexExistingHols].HolidayApproved == 1)
                {
                    RenderingCell.CssClass = "Approved";
                }
                else if (listExistingHolidays[indexExistingHols].HolidayApproved == 0)
                {
                    RenderingCell.CssClass = "Requested";
                }
                else
                {
                    RenderingCell.CssClass = "DisApproved";
                    RenderingDay.IsSelectable = true;
                }
            }

            // Check if RenderingDay is on list being currently Selected
            int indexSelectedHols = FindDate(listSelectedHolidays, RenderingDay.Date);
            if (indexSelectedHols >= 0)
            {
                RenderingCell.Text = listSelectedHolidays[indexSelectedHols].HolidayType.ToString();
                RenderingCell.CssClass = "Selected";
                RenderingCell.Attributes.Add("OnClick", e.SelectUrl);
            }
            else if (RenderingDay.IsSelectable)
            {
                // Reset Date Cell, if it was previously selected.
                RenderingCell.Style.Value = "";
            }
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            // Loop Through all Dates selected
            for (int i = 0; i < Calendar1.SelectedDates.Count; i++)
            {
                DateTime DateToCheck = Calendar1.SelectedDates[i];

                // Weekends aren't normally selectable but selecting a week or month does so
                // Same for Days past
                if ( !isWeekend(DateToCheck) && DateTime.Compare(DateTime.Today, DateToCheck) <= 0 )
                {
                    AdvanceDate(DateToCheck);
                }
            }

            // Clearing Selected Dates allows you to click on same date twice
            Calendar1.SelectedDates.Clear();
        }

        public static bool isWeekend(DateTime dtToValidate)
        {
            if (dtToValidate.DayOfWeek == DayOfWeek.Sunday || dtToValidate.DayOfWeek == DayOfWeek.Saturday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void AdvanceDate(DateTime ClickedDay)
        {
            var indexExisting = FindDate(listExistingHolidays, ClickedDay);
            if (indexExisting < 0)
            {
                var indexSelected = FindDate(listSelectedHolidays, ClickedDay);
                // If ClickDate is in List then advance it: H -> F -> Delete
                if (indexSelected >= 0)
                {
                    if (listSelectedHolidays[indexSelected].HolidayType == 'H')
                    {
                        listSelectedHolidays[indexSelected].HolidayType = 'F';

                        LeaveAllowance = LeaveAllowance - 0.5;
                        LeavePending = LeavePending + 0.5;
                    }
                    else if (listSelectedHolidays[indexSelected].HolidayType == 'F')
                    {
                        listSelectedHolidays.RemoveAt(indexSelected);

                        LeaveAllowance = LeaveAllowance + 1;
                        LeavePending = LeavePending - 1;
                    }
                }
                // If ClickDate is not in List then add it: H
                else
                {
                    listSelectedHolidays.Add(new ListHolidays
                    {
                        HolidayDate = ClickedDay,
                        HolidayType = 'H',
                        HolidayApproved = 0
                    });

                    LeaveAllowance = LeaveAllowance - 0.5;
                    LeavePending = LeavePending + 0.5;
                }

                // Update Display
                disAllowanceRemaining.Text = LeaveAllowance.ToString();
                disAllowancePending.Text = LeavePending.ToString();
            }
        }

        private int FindDate(List<ListHolidays> inList, DateTime Day)
        {
            int retIndex = -1; // If not found return -1
            try
            {
                ListHolidays Temp = inList.FirstOrDefault<ListHolidays>(Holiday => Holiday.HolidayDate == Day);

                // If not found, Temp will be null, null has an index of -1
                retIndex = inList.IndexOf(Temp);

                return retIndex;
            }
            catch {
                return retIndex;
            }
        }

        public static void GetExistingHolidays()
        {
            listExistingHolidays.Clear();

            // Output list of Public Holidays
            DataQueries dqHolidays = new DataQueries();
            DataTable dtHolidays = dqHolidays.GetExistingHolidaysByYear(UserID, DateTime.Now.Year);

            foreach (DataRow dr in dtHolidays.Rows)
            {
                listExistingHolidays.Add(new ListHolidays
                {
                    HolidayDate = (DateTime)dr["LeaveDate"],
                    HolidayType = dr["LeaveType"].ToString()[0],
                    HolidayApproved = Convert.ToInt32(dr["LeaveApproved"])
                });
            }
        }

        protected void UpdateAllowances()
        {
            DataQueries dqEmployeeAllowance = new DataQueries();
            DataTable dtEmployeeAllowance = dqEmployeeAllowance.GetStaffRequests(UserID, DateTime.Now.Year);

            string AnnualAllowance = dtEmployeeAllowance.Rows[0]["AnnualAllowance"].ToString();
            LeaveAllowance = Convert.ToDouble(dtEmployeeAllowance.Rows[0]["AllowanceRemainingPending"]);
            LeavePending = Convert.ToDouble(dtEmployeeAllowance.Rows[0]["LeaveOnRequest"]);

            disAnnualAllowance.Text = AnnualAllowance;
            disAllowanceRemaining.Text = LeaveAllowance.ToString();
            disAllowancePending.Text = LeavePending.ToString();
        }
    }
}