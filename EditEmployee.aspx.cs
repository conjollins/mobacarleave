﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Data;
using System.Text;
using System.Configuration;

namespace MobacarLeave
{
    public partial class _Employee : System.Web.UI.Page
    {
        public static List<ListMinions> MinionList = new List<ListMinions>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Verify User; otherwise boot them to MemberLogin
            if (Session["LoggedIn"] != null)
            {
                if (!(bool)Session["LoggedIn"])
                { Response.Redirect("~/MemberLogin.aspx"); }
            }
            else { Response.Redirect("~/MemberLogin.aspx"); }

            // Update Master Header
            Master.FindControl("LogOut").Visible = true;
            Master.FindControl("btnEditEmployee").Visible = false;
            Master.FindControl("btnApproveHolidays").Visible = false;
            Master.MasterReturn.Click += btnReturn_Click;
            Master.FindControl("btnReturn").Visible = true;
            Master.MasterPageLabel = "Mobacar Leave Management - Employee";

            PanelEmployee.Visible = false;

            if (!Page.IsPostBack)
            {
                // Do Stuff
                fillEmployeeList();
            }
        }

        protected void fillEmployeeList()
        {
            int UserID = Convert.ToInt32(Session["UserID"]);

            // Get All Employees
            DataQueries dqGetEmployees = new DataQueries();
            DataTable dtEmployees = dqGetEmployees.GetStaff(UserID, "Employees");

            if (dtEmployees != null)
            {
                cboEmployeeList.Items.Clear();

                for (int i = 0; dtEmployees.Rows.Count > i; i++)
                {
                    ListItem cboItm = new ListItem(
                        dtEmployees.Rows[i]["FullName"].ToString(),
                        dtEmployees.Rows[i]["ID"].ToString()
                        );
                    cboEmployeeList.Items.Add(cboItm);
                }
            }

            // Option to Add Employee
            cboEmployeeList.Items.Insert(0, new ListItem("Add Employee...", "Add"));
            // Initial value
            cboEmployeeList.Items.Insert(0, new ListItem("Please select an Employee...", "0"));
        }

        protected void cboEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Add Employee
            if (cboEmployeeList.SelectedValue == "Add")
            {
                PanelEmployee.Visible = true;

                fillManagerList();
                fillEmployeeLevel();
                
                inputEmployeeFirstName.Text = "";
                inputEmployeeLastName.Text = "";
                inputReportingUserName.Text = "";
                inputReportingPassword.Text = "";
                inputEmployeeEmail.Text = "";
                inputEmployeeAllowance.Text = "";
            }
            // Edit Employee
            else if (cboEmployeeList.SelectedValue != "0")
            {
                int EmployeeID = Convert.ToInt32(cboEmployeeList.SelectedValue);
                PanelEmployee.Visible = true;

                fillManagerList();
                fillEmployeeLevel();

                DataQueries dqGetEmployeeDetails = new DataQueries();
                DataTable dtGetEmployeeDetails = dqGetEmployeeDetails.GetStaff(EmployeeID, "EmployeeDetails");

                if (dtGetEmployeeDetails != null)
                {
                    string FirstName = dtGetEmployeeDetails.Rows[0]["FirstName"].ToString();
                    string LastName = dtGetEmployeeDetails.Rows[0]["LastName"].ToString();

                    inputEmployeeFirstName.Text = FirstName;
                    inputEmployeeLastName.Text = LastName;

                    string UserName = dtGetEmployeeDetails.Rows[0]["UserName"].ToString();
                    string Password = dtGetEmployeeDetails.Rows[0]["Password"].ToString();

                    inputReportingUserName.Text = UserName;
                    inputReportingPassword.Text = Password;

                    string EmailAddress = dtGetEmployeeDetails.Rows[0]["EmailAddress"].ToString();
                    string AccessLevel = dtGetEmployeeDetails.Rows[0]["AccessLevel"].ToString();

                    inputEmployeeEmail.Text = EmailAddress;
                    cboEmployeeLevelList.SelectedValue = AccessLevel;

                    string AnnualAllowance = dtGetEmployeeDetails.Rows[0]["AnnualAllowance"].ToString();
                    string ReportsTo = dtGetEmployeeDetails.Rows[0]["ReportsTo"].ToString();

                    inputEmployeeAllowance.Text = AnnualAllowance;
                    cboManagerList.SelectedValue = ReportsTo;
                }
            }
            //Do nothing
            else
            {
                PanelEmployee.Visible = false;
            }
        }

        protected void fillManagerList()
        {
            // Get All Managers
            int UserID = Convert.ToInt32(Session["UserID"]);

            DataQueries dqGetManagers = new DataQueries();
            DataTable dtManagers = dqGetManagers.GetStaff(UserID, "AllManagers");

            if (dtManagers != null)
            {
                cboManagerList.Items.Clear();
                for (int i = 0; dtManagers.Rows.Count > i; i++)
                {
                    ListItem cboItm = new ListItem(
                        dtManagers.Rows[i]["FullName"].ToString(),
                        dtManagers.Rows[i]["ID"].ToString()
                        );
                    cboManagerList.Items.Add(cboItm);
                }
            }
        }

        protected void fillEmployeeLevel()
        {
            string SelectedEmployee = cboEmployeeList.SelectedValue;
            
            // Get Employee Levels
            int UserID = Convert.ToInt32(Session["UserID"]);

            DataQueries dqGetEmployeeLevels = new DataQueries();
            DataTable dtEmployeeLevels = dqGetEmployeeLevels.GetEmployeeLevels("ADD", UserID);

            if (dtEmployeeLevels != null)
            {
                cboEmployeeLevelList.Items.Clear();
                for (int i = 0; dtEmployeeLevels.Rows.Count > i; i++)
                {
                    ListItem cboItm = new ListItem(
                        dtEmployeeLevels.Rows[i]["Level"].ToString(),
                        dtEmployeeLevels.Rows[i]["ID"].ToString()
                        );
                    cboEmployeeLevelList.Items.Add(cboItm);
                }
            }
        }

        protected void btnSaveEmployee_Click(object sender, EventArgs e)
        {
            // Save Details
            int EmployeeID = 0;
            if (cboEmployeeList.SelectedValue != "0" && cboEmployeeList.SelectedValue != "Add")
            {
                EmployeeID = Convert.ToInt32(cboEmployeeList.SelectedValue);
            }

            string FirstName = inputEmployeeFirstName.Text;
            string LastName = inputEmployeeLastName.Text;

            string UserName = inputReportingUserName.Text;
            string Password = inputReportingPassword.Text;

            string EmailAddress = inputEmployeeEmail.Text;
            int AccessLevel = Convert.ToInt32(cboEmployeeLevelList.SelectedValue);

            int ReportsTo = Convert.ToInt32(cboManagerList.SelectedValue);

            // Save Employee
            if (checkIsNumber(inputEmployeeAllowance.Text))
            {
                double AnnualAllowance = Convert.ToDouble(inputEmployeeAllowance.Text);

                DataQueries dqSaveEmployee = new DataQueries();
                DataTable dtSaveEmployee = dqSaveEmployee.SaveEmployee( EmployeeID
                                                                      , FirstName
                                                                      , LastName
                                                                      , UserName
                                                                      , Password
                                                                      , EmailAddress
                                                                      , AccessLevel
                                                                      , AnnualAllowance
                                                                      , ReportsTo);

                if (dtSaveEmployee != null)
                {
                    string Result = dtSaveEmployee.Rows[0]["Result"].ToString();

                    // Save Result
                    PopUp(Result);
                }
                else 
                {
                    // Save Failed
                    PopUp("Save Failed");
                }
            }

            // Reset Employee List
            fillEmployeeList();
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/AddLeave.aspx");
        }

        protected bool checkIsNumber(string testString)
        {
            float output;
            return float.TryParse(testString, out output);
        }
        
        protected void PopUp(string Message)
        {
            Response.Write("<script>alert('" + Message + "');</script>");
        }
    }
}