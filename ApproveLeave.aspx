﻿<%@ Page Title="Mobacar Leave Management" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="ApproveLeave.aspx.cs" Inherits="MobacarLeave._Approve"  %>
    
<%@ MasterType VirtualPath="Site.Master" %>

<%@ Register Src="~/Controls/ApproveHolidays.ascx" TagName="ApproveHolidays" TagPrefix="uc" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    table.HolidayCalendar td a {
        display: inline-block;
        height:100%;
        width:100%;
        text-decoration: none !important;
    }
    table.HolidayCalendar td.Selectable:hover {
        background-color:Silver;
    }
    table.HolidayCalendar td.Requested {
        background-color:#deff00;
    }
    table.HolidayCalendar td.Requested:hover {
        cursor:pointer;
        background-color:#00FF21;
    }
    table.HolidayCalendar td.PreApproved {
        background-color:#00FF21;
    }
    table.HolidayCalendar td.DisApproved {
        background-color:#ff005f;
    }
    table.HolidayCalendar td.DisApproved:hover {
        cursor:pointer;
    }
    table.HolidayCalendar td.Approved {
        background-color:#00FF21;
    }
    table.HolidayCalendar td.Approved:hover {
        cursor:pointer;
    }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel 
        ID="PanelHolidayRequest" 
        runat="server" 
        DefaultButton="btnApproveHolidayRequest"
        Style="width: 600px; margin: auto;"
        >
        <div class="row">
            <div class="col-md-6" style="padding-top: 13px;">
                <uc:ApproveHolidays 
                    id="datePicker" 
                    runat="server" 
                    Style="align: center;"
                    />
            </div>
            <div class="col-md-6" style="padding-top: 13px;">
                <div class="row">
                    <asp:Label
                        ID="lblEmployeeList"
                        runat="server"
                        style="font-weight:bold;"
                        Text="Minion List"
                        />
                    <br/>
                    <asp:DropDownList
                        ID="cboEmployeeList"
                        runat="server"
                        Class="form-control"
                        AutoPostBack="True"
                        OnSelectedIndexChanged="cboEmployeeList_SelectedIndexChanged"
                        />
                </div>
                <div class="row" style="padding-top: 10px;">
                    <asp:Label
                        ID="lblLeaveTypeList"
                        runat="server"
                        style="font-weight:bold;"
                        Text="Leave Type"
                        />
                    <br/>
                    <asp:DropDownList
                        ID="cboLeaveTypeList"
                        runat="server"
                        Class="form-control"
                        AutoPostBack="True"
                        OnSelectedIndexChanged="cboLeaveTypeList_SelectedIndexChanged"
                        />
                </div>
                <div class="row" style="padding-top: 13px;">
                    <asp:Button 
                        ID="btnApproveHolidayRequest" 
                        runat="server" 
                        CssClass="btn btn-primary" 
                        Style="width: 100%;"
                        Text="Save Leave" 
                        UseSubmitBehavior="false" 
                        onClick="btnApproveHolidayRequest_Click"
                        />
                </div>
                <div class="row" style="padding-top: 13px;">
                    <asp:Image ID="imgSubmittedHoliday" runat="server" imageurl="Images/SubmittedDay.png" />
                    <asp:Label ID="lblSubmittedHoliday" runat="server" Text="Submitted Leave" />
                    <br/>
                    <asp:Image ID="imgApprovedHoliday" runat="server" imageurl="Images/ApprovedDay.png" />
                    <asp:Label ID="lblApprovedHoliday" runat="server" Text="Approved Leave" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <br />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="Assets/js/bootstrap.min.js"></script>
</asp:Content>
