﻿<%@ Page 
    Title="Mobacar Leave Management" 
    Language="C#" 
    MasterPageFile="~/Site.Master" 
    AutoEventWireup="true"
    CodeBehind="MemberLogin.aspx.cs" 
    Inherits="MobacarLeave._Login"
    %>

<%@ MasterType VirtualPath="Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</asp:Content>

<asp:Content ID="LoginPage" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="PanelLogin" runat="server" DefaultButton="LoginButton" style="margin: 10px auto; width: 300px; text-align:center;">
        <asp:TextBox 
            ID="inputUserName" 
            runat="server" 
            CssClass="form-control" 
            PlaceHolder="User Name"
            style="margin: 10px;"
            />
        <asp:TextBox
            ID="inputPassword"
            runat="server"
            CssClass="form-control"
            TextMode="Password"
            PlaceHolder="Password"
            style="margin: 10px;"
            />
        <asp:Button
            ID="LoginButton"
            runat="server"
            CssClass="btn btn-lg btn-primary btn-block"
            Text="Log In"
            style="margin: 10px;"
            OnClick="btnLoginButton_Click"
            />
        <asp:Literal
            ID="FailureText"
            runat="server"
            EnableViewState="False"
            />
    </asp:Panel>
</asp:Content>
