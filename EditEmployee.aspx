﻿<%@ Page Title="Mobacar Leave Management" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeBehind="EditEmployee.aspx.cs" Inherits="MobacarLeave._Employee"  %>
    
<%@ MasterType VirtualPath="Site.Master" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:Panel ID="PanelAddEdit" runat="server" Style="width: 800px; margin: auto;">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-3" style="padding-top:6px;">
                <asp:Label ID="lblEmployeeList" runat="server" style="font-weight:bold;" Text="Select an Employee" />
            </div>
            <div class="col-md-5">
                <asp:DropDownList 
                    ID="cboEmployeeList" runat="server" Class="form-control" AutoPostBack="True"
                    OnSelectedIndexChanged="cboEmployeeList_SelectedIndexChanged" />
            </div>
            <div class="col-md-2"></div>
        </div>
    </asp:Panel>
    <hr/>
    <asp:Panel ID="PanelEmployee" runat="server" Style="width: 800px; margin: auto;" visibility="true">
        <div class="row">
            <div class="col-md-2" style="padding-top:6px;">
                <asp:Label ID="lblEmployeeFirstName" runat="server" style="font-weight:bold;" Text="First Name" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputEmployeeFirstName" runat="server" CssClass="form-control" PlaceHolder="First Name" MaxLength="100"/>
            </div>
            <div class="col-md-2" style="padding-top:6px;">
                <asp:Label ID="lblEmployeeLastName" runat="server" style="font-weight:bold;" Text="Last Name" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputEmployeeLastName" runat="server" CssClass="form-control" PlaceHolder="Last Name" MaxLength="100"/>
            </div>
        </div>
        <div class="row" style="padding-top:10px;">
            <div class="col-md-2" style="padding-top:6px;">
                <asp:Label ID="lblReportingUserName" runat="server" style="font-weight:bold;" Text="User Name" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputReportingUserName" runat="server" CssClass="form-control" PlaceHolder="User Name" MaxLength="100"/>
            </div>
            <div class="col-md-2" style="padding-top:6px;">
                <asp:Label ID="lblReportingPassword" runat="server" style="font-weight:bold;" Text="Password" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputReportingPassword" runat="server" CssClass="form-control" PlaceHolder="Password" MaxLength="100"/>
            </div>
        </div>
        <div class="row" style="padding-top:10px;">
            <div class="col-md-2">
                <asp:Label ID="lblEmployeeEmail" runat="server" style="font-weight:bold;" Text="Employee Email" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputEmployeeEmail" runat="server" CssClass="form-control" PlaceHolder="Employee Email" MaxLength="100"/>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblEmployeeLevel" runat="server" style="font-weight:bold;" Text="Employee Level" />
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="cboEmployeeLevelList" runat="server" Class="form-control" />
            </div>
        </div>
        <div class="row" style="padding-top:10px;">
            <div class="col-md-2">
                <asp:Label ID="lblEmployeeAllowance" runat="server" style="font-weight:bold;" Text="Employee Allowance" />
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="inputEmployeeAllowance" runat="server" CssClass="form-control" PlaceHolder="Employee Allowance" MaxLength="100"/>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblEmployeeManager" runat="server" style="font-weight:bold;" Text="Employee Manager" />
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="cboManagerList" runat="server" Class="form-control" />
            </div>
        </div>
        <hr/>
        <div class="row" style="padding-top:10px;">
            <asp:Button 
                ID="btnSaveEmployee" 
                runat="server" 
                CssClass="btn btn-primary" 
                Style="width: 100%;"
                Text="Save Employee" 
                UseSubmitBehavior="false" 
                onClick="btnSaveEmployee_Click"
                />
        </div>
    </asp:Panel>
    <br />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="Assets/js/bootstrap.min.js"></script>
</asp:Content>
