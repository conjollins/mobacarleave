﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;

namespace MobacarLeave
{
    public partial class _Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.MasterPageLabel = "Mobacar Leave Management - Log In";
            Session["LoggedIn"] = false;
            Session["UserID"] = "";
            Session["UserName"] = "";
            Session["AccessLevel"] = "";
            Session["EmailAddress"] = "";
            Session["FirstName"] = "";
            Session["LastName"] = "";
        }

        protected void btnLoginButton_Click(object sender, EventArgs e)
        {
            // Check Login Details
            try
            {
                String UserName = inputUserName.Text.ToString();
                String Password = inputPassword.Text.ToString();
                
                // Get Content
                DataQueries dqLogin = new DataQueries();
                DataTable dtLogin = new DataTable();
                dtLogin = dqLogin.LoginUser(UserName, Password);

                if (dtLogin != null && dtLogin.Rows.Count == 1)
                {
                    // Create User Session
                    Session["LoggedIn"] = true;
                    Session["UserID"] = dtLogin.Rows[0]["ID"].ToString();
                    Session["UserName"] = dtLogin.Rows[0]["UserName"].ToString();
                    Session["AccessLevel"] = dtLogin.Rows[0]["AccessLevel"].ToString();
                    Session["EmailAddress"] = dtLogin.Rows[0]["EmailAddress"].ToString();
                    Session["FirstName"] = dtLogin.Rows[0]["FirstName"].ToString();
                    Session["LastName"] = dtLogin.Rows[0]["LastName"].ToString();

                    // Move on
                    Response.Redirect("AddLeave.aspx", false);
                }
                else 
                {
                    FailureText.Text = "You do not have access to this page";
                }

            }
            catch (Exception ex)
            {
                // Output Error Message: Invalid Login
                FailureText.Text = "You do not have access to this page";
            }
        }
    }
}
